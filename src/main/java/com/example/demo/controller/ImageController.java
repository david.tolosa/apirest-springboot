package com.example.demo.controller;

import com.example.demo.model.Imagen;
import com.example.demo.service.ImagenService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/imagen")
public class ImageController {

    private final ImagenService imagenService;

    @Operation(summary = "Obtener todas las imagenes")
    @GetMapping
    public ResponseEntity<Collection<Imagen>> getAllImagenes() {
        return ResponseEntity.status(HttpStatus.OK).body(imagenService.getAllEntities());
    }

    @Operation(summary = "Crear imagen")
    @PostMapping
    public ResponseEntity<String> crearImagen(@RequestBody Imagen imagen){
        imagenService.createEntity(imagen);
        return ResponseEntity.status(HttpStatus.CREATED).body("imagen " + imagen.getNombre()+" creada");
    }

    @Operation(summary = "Obtener imagen por id")
    @GetMapping("/{id}")
    public ResponseEntity<Imagen> getImagenById(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(imagenService.getEntityById(id));
    }

    @Operation(summary = "Actualizar imagen")
    @PutMapping("/{id}")
    public ResponseEntity<String> updateImagen(@PathVariable Long id, @RequestBody Imagen imagen){
        imagenService.updateEntity(imagen);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("imagen " + imagen.getNombre()+" actualizada");
    }

    @Operation(summary = "Eliminar imagen")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteImagen(@PathVariable Long id){
        imagenService.deleteEntity(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("imagen eliminada");
    }
}
