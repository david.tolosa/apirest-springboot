package com.example.demo.controller;

import com.example.demo.dtos.PersonaDto;
import com.example.demo.mapper.PersonaMapperPersonaDto;
import com.example.demo.model.Persona;
import com.example.demo.service.PersonaService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;


@RestController
@RequiredArgsConstructor
@RequestMapping("/persona")
public class PersonaController {

    private final PersonaService personaService;
    private final PersonaMapperPersonaDto personaMapperPersonaDto;

    @Operation(summary = "Crear persona")
    @PostMapping
    public ResponseEntity<String> crearPersona(@RequestBody PersonaDto persona){
        System.out.println(persona.toString());
        Persona p = personaMapperPersonaDto.personaDtoToPersona(persona);
        p.getImagen().setId(null);
        System.out.println(p.toString());

        personaService.createEntity(p);
        return ResponseEntity.status(HttpStatus.CREATED).body("persona " + persona.getNombre()+" "+persona.getApellido()+" creada");
    }

    @Operation(summary = "Obtener todas las personas")
    @GetMapping
    public ResponseEntity<Collection<Persona>> getPersonas(){
        return ResponseEntity.status(HttpStatus.OK).body(personaService.getAllEntities());
    }

    @Operation(summary = "Obtener persona por id")
    @GetMapping("{id}")
    public ResponseEntity<Persona> getPersonaById(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(personaService.getEntityById(id));
    }

    @Operation(summary = "Obtener persona por nombre")
    @GetMapping("/nombre")
    public ResponseEntity<Persona> getPersonaByNombre(@RequestParam String nombre){
        return ResponseEntity.status(HttpStatus.OK).body(personaService.getEntityByNombre(nombre));
    }

    @Operation(summary = "Actualizar persona")
    @PutMapping("{id}")
    public ResponseEntity<String> updatePersona(@PathVariable Long id, @RequestBody Persona persona){
        personaService.updateEntity(persona);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("persona " + persona.getNombre()+" "+persona.getApellido()+" actualizada");
    }

    @Operation(summary = "Eliminar persona")
    @DeleteMapping("{id}")
    public ResponseEntity<String> deletePersona(@PathVariable Long id){
        personaService.deleteEntity(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("persona eliminada");
    }

}
