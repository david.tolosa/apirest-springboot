package com.example.demo.dtos;

import lombok.Data;

@Data
public class ImagenDto {
    private String nombre;
    private String imagenBase64;
}
