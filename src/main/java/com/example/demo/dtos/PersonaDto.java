package com.example.demo.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class PersonaDto implements Serializable {
    private String nombre;
    private String apellido;
    private ImagenDto imagen;
}
