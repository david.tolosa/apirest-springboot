package com.example.demo.exception;

public class ImagenNotFoundException extends Exception {
    public ImagenNotFoundException(String message) {
        super(message);
    }
}
