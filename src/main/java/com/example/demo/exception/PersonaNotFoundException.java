package com.example.demo.exception;

public class PersonaNotFoundException extends Exception {
    public PersonaNotFoundException(String message) {
        super(message);
    }
}
