package com.example.demo.mapper;

import com.example.demo.dtos.ImagenDto;
import com.example.demo.model.Imagen;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE,unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface ImagenMapperImagenDto {
    Imagen imagenDtoToImagen(ImagenDto dto);
    ImagenDto imagenToImagenDto(Imagen imagen);
}
