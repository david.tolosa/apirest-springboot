package com.example.demo.mapper;

import com.example.demo.dtos.PersonaDto;
import com.example.demo.model.Persona;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE,unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface PersonaMapperPersonaDto {
    Persona personaDtoToPersona(PersonaDto dto);
    PersonaDto PersonaToPersonaDto(Persona persona);
}
