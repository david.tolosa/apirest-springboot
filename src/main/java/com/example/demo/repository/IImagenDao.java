package com.example.demo.repository;

import com.example.demo.model.Imagen;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IImagenDao extends CrudRepository<Imagen,Long> {

}
