package com.example.demo.repository;

import com.example.demo.model.Persona;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IPersonaDao extends CrudRepository<Persona,Long> {

    Persona findByNombre(String nombre);

}
