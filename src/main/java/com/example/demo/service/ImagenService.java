package com.example.demo.service;

import com.example.demo.model.Imagen;
import com.example.demo.repository.IImagenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class ImagenService implements ServiceCRUD<Imagen,Long>{

    @Autowired
    private IImagenDao imagenRepo;

    @Override
    public Imagen createEntity(Imagen entity) {
        return imagenRepo.save(entity);
    }

    @Override
    public Collection<Imagen> getAllEntities() {
        return (Collection<Imagen>) imagenRepo.findAll();
    }

    @Override
    public void deleteEntity(Long id) {
        imagenRepo.deleteById(id);
    }

    @Override
    public void updateEntity(Imagen entity) {
        if(imagenRepo.existsById(entity.getId())){
            imagenRepo.save(entity);
        }
    }

    @Override
    public Imagen getEntityById(Long id) {
        return imagenRepo.findById(id).orElseThrow();
    }
}
