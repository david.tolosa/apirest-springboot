package com.example.demo.service;

import com.example.demo.model.Persona;
import com.example.demo.repository.IPersonaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class PersonaService implements ServiceCRUD<Persona,Long> {

    @Autowired
    private IPersonaDao personaRepo;

    @Override
    public Persona createEntity(Persona entity) {
        return personaRepo.save(entity);
    }

    @Override
    public Collection<Persona> getAllEntities() {
        return (Collection<Persona>) personaRepo.findAll();
    }

    @Override
    public void deleteEntity(Long id) {
        personaRepo.deleteById(id);
    }

    @Override
    public void updateEntity(Persona entity) {
        if(personaRepo.existsById(entity.getId())){
            personaRepo.save(entity);
        }
    }

    @Override
    public Persona getEntityById(Long id) {
        return personaRepo.findById(id).orElseThrow();
    }

    public Persona getEntityByNombre(String nombre){
        return personaRepo.findByNombre(nombre);
    }

}
